import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { CorporativosRoutingModule } from "./corporativos-routing.module";

import { CorporativosComponent } from "./corporativos.component";
import { CorporativosServices } from 'app/shared/services/corporativos.service';

@NgModule({
  imports: [
    CommonModule,
    CorporativosRoutingModule,
    NgxDatatableModule
  ],
  exports: [],
  declarations: [
    CorporativosComponent
  ],
  providers: [
    CorporativosServices
  ],
})
export class CorporativosModule { }