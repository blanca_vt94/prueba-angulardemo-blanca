import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent, ColumnMode} from '@swimlane/ngx-datatable';
import { CorporativosServices } from 'app/shared/services/corporativos.service';

@Component({
  selector: 'app-corporativos',
  templateUrl: './corporativos.component.html',
  styleUrls: [
    "./users-list.component.scss",
  ],
})
export class CorporativosComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  
  //Row
  public limitRef = 10;
  public ColumnMode = ColumnMode;
   
  rows=[];

  private tempData=[];

  constructor (private corp: CorporativosServices) {   

    this.corp.getCorporativos()
      .subscribe((data:any) =>{        
        this.rows=data;
        console.log(this.rows);
      }); 
      
      this.tempData=this.rows;
      }


 // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * filterUpdate
   *
   * @param event
   */
  filterUpdate(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempData.filter(function (d) {
      return d.Corporativo.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
   this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

   /**
   * updateLimit
   *
   * @param limit
   */
  updateLimit(limit) {
    this.limitRef = limit.target.value;
  }

  ngOnInit(): void {
  }
}
