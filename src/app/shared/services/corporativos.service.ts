import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { data } from '../data/smart-data-table';

import { map } from 'rxjs/operators';

@Injectable({
    providedIn: "root"
  })
export class CorporativosServices{
    public apiURL=environment.apiURL+'/corporativos';
    public auth_token='Bearer '+localStorage.getItem('tokenscloud');

    constructor(private http: HttpClient){ 
        console.log('Servicio listo');       
    } 
    
    headers: HttpHeaders = new HttpHeaders({
        'Authorization': this.auth_token});

    getCorporativos(){       
        
        return this.http.get(this.apiURL, {headers:this.headers});
        //.pipe( map(data=>data['data'].items)) ;         
    }
   

    

    


}